from django.db import models

# Create your models here.
class Mplan(models.Model):
    name = models.CharField(max_length=120)
    date = models.DateTimeField("Meal Will be served on")
    owner = models.ForeignKey(
        "AUTH_USER_MODEL",
        on_delete=models.CASCADE,
    )
    recipes = models.ManyToManyField("recipes.Recipe", related_name="recipes")
