from django.shortcuts import render
from django.shortcuts import redirect
from django.urls import reverse_lazy
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, DeleteView, UpdateView
from django.views.generic.list import ListView
from django.contrib.auth import views as auth_views

from meal_plan.models import Mplan

# Create your views here.


class MealPlanListView(auth_views, ListView):
    model = Mplan
    template_name = "meal_plan/"
    pageinate_by = 2
