from django.urls import path
from meal_plan.views import MealPlanListView
from django.contrib.auth import views as auth_views

urlpatterns = [
path("meal_plans/", MealPlanListView.as_view(), name='mealplan_list'),
path('meal_plans/create/', MealplanCreateView.as_view(), name='mealplan_create'),
path('meal_plans/<int:pk>/', MealPlanDetailsView.as_view(), name = 'mealplan_details'),
path('meal_plans/<int:pk>/edit/', MealPlanEditView.as_view(), name = 'mealplan_edit'),
path('meal_plans/<int:pk>/delete/', MealPlanDeleteView.as_view(), name= 'mealplan_delete')
path("accounts/login/", auth_views.LoginView.as_view(), name="login"),
]